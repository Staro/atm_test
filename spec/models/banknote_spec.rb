require 'rails_helper'

RSpec.describe Banknote, type: :model do
  describe '.total_amount' do
    subject { described_class.total_amount }

    before { create :banknote, nominal: 50, quantity: 2 }

    it 'returns valid amount' do
      expect(subject).to eq(100)
    end
  end
end