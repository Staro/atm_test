require 'rails_helper'

RSpec.describe 'Banknotes management', type: :request do
  context 'GET /api/v1/banknotes' do
    subject { get '/api/v1/banknotes', params: params }

    let(:params) { nil }

    it 'returns an 400 error when amount not present' do
      subject

      expect(response.status).to eq(400)
    end

    context 'with provided amount' do
      let(:params) { { amount: 50 } }

      it 'returns an 406 error when not enough money' do
        subject

        expect(response.status).to eq(406)
        expect(JSON.parse(response.body)['error']).to eq('Amount bigger than currently stored.')
      end

      context 'with money stored' do
        let!(:banknote) { create :banknote, nominal: 50, quantity: 1 }

        let(:expected_response) { { '50' => 1 } }

        it 'returns hash of banknotes' do
          subject

          expect(response.status).to eq(200)
          expect(JSON.parse(response.body)).to eq(expected_response)
        end

        it 'changes quantity of banknotes' do
          expect { subject }.to change { banknote.reload.quantity }.from(1).to(0)
        end

        context 'when amount can not be composed' do
          let(:params) { { amount: 25 } }

          it 'returns error' do
            subject

            expect(response.status).to eq(422)
            expect(JSON.parse(response.body)['error']).to eq('["Can\'t provide amount with current nominals"]')
          end
        end
      end
    end
  end

  context 'POST /api/v1/banknotes' do
    subject { post '/api/v1/banknotes', params: params }

    let(:params) { nil }

    it 'returns an 400 error when banknotes are not in params' do
      subject

      expect(response.status).to eq(400)
    end

    context 'with banknotes params present' do
      let(:params) { { banknotes: [{ nominal: 50, quantity: 1 }] } }

      it 'returns an 201' do
        subject

        expect(response.status).to eq(201)
      end

      it 'creates banknote' do
        expect { subject }.to change { Banknote.count }.from(0).to(1)
      end
    end

    context 'with invalid nominal' do
      let(:params) { { banknotes: [{ nominal: 700, quantity: 1 }] } }

      it 'returns an 400' do
        subject

        expect(response.status).to eq(400)
      end
    end
  end
end