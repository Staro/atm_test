require 'rails_helper'

RSpec.describe BanknotesExtractor, type: :service do
  describe '#extract' do
    subject { described_class.new.extract(amount) }

    let!(:banknote_50) { create :banknote, nominal: 50, quantity: 1 }

    let(:amount)          { 50 }
    let(:expected_result) { { 50 => 1 } }

    it 'changes quantity of banknote' do
      expect { subject }.to change { banknote_50.reload.quantity }.from(1).to(0)
    end

    it 'returns correct banknotes hash' do
      expect(subject).to eq(expected_result)
    end

    context 'when amount can not be provided by current banknotes' do
      let(:amount) { 25 }

      it 'should set error' do
        expect(subject[0]).to eq("Can't provide amount with current nominals")
      end
    end

    context 'when amount more than stored' do
      let(:amount) { 200 }

      it 'should set error' do
        expect(subject[0]).to eq("Amount bigger than currently stored.")
      end
    end
  end
end