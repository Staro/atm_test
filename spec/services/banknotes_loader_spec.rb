require 'rails_helper'

RSpec.describe BanknotesLoader, type: :service do
  describe '#load' do
    subject { described_class.new.load(banknotes) }

    let(:banknotes) {}

    it 'should not change count of banknotes' do
      expect { subject }.to_not change { Banknote.count }
    end

    context 'with provided banknotes' do
      let(:banknotes) { [{ nominal: 1, quantity: 1 }] }

      it 'should create banknote' do
        expect { subject }.to change { Banknote.count }.from(0).to(1)
      end

      context 'with already created banknote' do
        let!(:banknote) { create :banknote, nominal: 1 }

        it 'changes banknote quantity' do
          expect { subject }.to change { banknote.reload.quantity }.by(1)
        end
      end
    end
  end
end