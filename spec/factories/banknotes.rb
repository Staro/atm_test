FactoryBot.define do
  factory :banknote do
    nominal  Banknote::ALLOWED_NOMINALS.sample
    quantity Random.rand(100)
  end
end