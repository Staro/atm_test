# README

Basic atm app.

Example creatin banknotes request:

```
curl --request POST \
  --url 'http://localhost:3000/api/v1/banknotes' \
  --header 'content-type: application/json' \
  --data '{
	"banknotes": [
		{
			"nominal": 5,
			"quantity": 2
		},
		{
			"nominal": 5,
			"quantity": 2
		},
		{
			"nominal": 1,
			"quantity": 200
		},
		{
			"nominal": 25,
			"quantity": 50
		}
	]
}'

```

Example getting amount request:

```
curl --request GET \
  --url 'http://localhost:3000/api/v1/banknotes?amount=200' \
  --header 'content-type: application/json'
```
