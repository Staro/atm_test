# frozen_string_literal: true

module Atm
  class API < Grape::API
    mount Atm::V1::Banknotes
  end
end