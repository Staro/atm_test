# frozen_string_literal: true

module Atm
  module V1
    class Banknotes < Grape::API
      version 'v1', using: :path
      format :json
      prefix :api

      resource :banknotes do
        params do
          group :banknotes, type: Array, desc: 'An array of banknotes' do
            requires :nominal, type: Integer, values: Banknote::ALLOWED_NOMINALS
            requires :quantity, type: Integer
          end
        end

        desc 'Load banknotes to atm'

        post do
          loader = BanknotesLoader.new
          loader.load(params[:banknotes])
        end

        desc 'Returns requested amount as banknotes hash'

        params do
          requires :amount, type: Integer
        end

        get do
          error! 'Amount bigger than currently stored.', 406 if params[:amount] > Banknote.total_amount

          extractor = BanknotesExtractor.new
          banknotes_to_return = extractor.extract(params[:amount])

          error! extractor.errors.to_s, 422 if extractor.errors.any?

          banknotes_to_return
        end
      end
    end
  end
end