# frozen_string_literal: true

class Banknote < ApplicationRecord
  ALLOWED_NOMINALS = [1, 2, 5, 10, 25, 50].freeze

  validates :nominal, inclusion: { in: ALLOWED_NOMINALS, message: '%{value} is not allowed nominal' }

  def self.total_amount
    sum('nominal * quantity')
  end
end
