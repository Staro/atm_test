# frozen_string_literal: true

class BanknotesLoader
  def load(banknotes)
    return unless banknotes.present?

    Banknote.transaction do
      banknotes.each do |banknote|
        stored_banknote = Banknote.find_or_create_by(nominal: banknote[:nominal])
        stored_banknote.increment!(:quantity, banknote[:quantity])
      end
    end
  end
end