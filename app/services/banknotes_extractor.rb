# frozen_string_literal: true

class BanknotesExtractor
  def extract(amount)
    return errors << 'Amount bigger than currently stored.' if amount > Banknote.total_amount

    banknotes_to_return = compose_banknotes(amount)

    return errors << "Can't provide amount with current nominals" if return_amount(banknotes_to_return) != amount

    decrement_amounts(banknotes_to_return)

    banknotes_to_return
  end

  def errors
    @errors ||= []
  end

  private

  def return_amount(banknotes_to_return)
    banknotes_to_return.map { |nominal, quantity| nominal * quantity }.sum
  end

  def compose_banknotes(amount)
    Banknote.all.order(nominal: :desc).inject({}) do |banknotes, banknote|
      return banknotes if amount.zero?

      quantity = (amount / banknote.nominal) > banknote.quantity ? banknote.quantity : (amount / banknote.nominal)

      next banknotes if quantity.zero?

      amount -= quantity * banknote.nominal

      banknotes[banknote.nominal] = quantity

      banknotes
    end
  end

  def decrement_amounts(banknotes)
    Banknote.transaction do
      banknotes.each do |nominal, quantity|
        stored_banknote = Banknote.find_or_create_by(nominal: nominal)
        stored_banknote.decrement!(:quantity, quantity)
      end
    end
  end
end