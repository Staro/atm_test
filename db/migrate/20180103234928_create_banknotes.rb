class CreateBanknotes < ActiveRecord::Migration[5.1]
  def change
    create_table :banknotes do |t|
      t.integer :nominal, null: false, index: true
      t.integer :quantity, null: false, default: 0

      t.timestamps
    end
  end
end
